/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package fr.verteego.processors;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import org.apache.nifi.annotation.behavior.*;
import org.apache.nifi.annotation.behavior.InputRequirement.Requirement;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.logging.ComponentLog;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.util.StandardValidators;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import static java.lang.Integer.parseInt;

@EventDriven
@SideEffectFree
@InputRequirement(Requirement.INPUT_REQUIRED)
@Tags({"openrefine", "clean", "excel", "ingest", "ingress"})
@CapabilityDescription("Apply a set of openrefine operations on an Excel file and create a Csv file output as a Flowfile")
@WritesAttributes({
        @WritesAttribute(attribute = "filename", description = "The filename is set to the name specified in the output " +
                "filename property")
})
public class CleanOpenRefineExcel extends AbstractProcessor {

    public static final PropertyDescriptor URL = new PropertyDescriptor.Builder()
            .name("URL")
            .description("The URL to POST to. The first part of the URL must be static. However, the path of the URL may be defined using the Attribute Expression Language. "
                    + "For example, https://${hostname} is not valid, but http://1.1.1.1 is valid.")
            .required(true)
            .addValidator(StandardValidators.createRegexMatchingValidator(Pattern.compile("https?\\://.*")))
            .addValidator(StandardValidators.URL_VALIDATOR)
            .expressionLanguageSupported(true)
            .build();

    public static final PropertyDescriptor PORT = new PropertyDescriptor.Builder()
            .name("Port")
            .description("The port that the remote openrefine server is listening on for file transfers")
            .addValidator(StandardValidators.PORT_VALIDATOR)
            .required(true)
            .defaultValue("3333")
            .build();

    public static final PropertyDescriptor JSON = new PropertyDescriptor.Builder()
            .name("Operations JSON")
            .description("JSON string containing openrefine operations to apply to input file")
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .required(true)
            .build();

    public static final PropertyDescriptor PROJECT_NAME = new PropertyDescriptor.Builder()
            .name("Project Name")
            .description("The project name to be used in openrefine")
            .expressionLanguageSupported(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .required(true)
            .build();

    public static final PropertyDescriptor DELETE_PROJECT = new PropertyDescriptor.Builder()
            .name("Delete project afterwards")
            .description("Whether to delete project after the JSON operations has been applied or not")
            .addValidator(StandardValidators.BOOLEAN_VALIDATOR)
            .required(true)
            .allowableValues("true", "false")
            .defaultValue("true")
            .build();

    public static final PropertyDescriptor OUTPUT_FILENAME = new PropertyDescriptor.Builder()
            .name("Output filename")
            .description("The output filename that overrides the output Folwfile filename attribute")
            .expressionLanguageSupported(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .required(true)
            .build();

    public static final PropertyDescriptor OUTPUT_FILE_EXTENSION = new PropertyDescriptor.Builder()
            .name("Output file extension")
            .description("Output filename extension")
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .required(true)
            .allowableValues("csv", "tsv")
            .defaultValue("csv")
            .build();

    public static final PropertyDescriptor IGNORE_FIRST = new PropertyDescriptor.Builder()
            .name("Ignore first line(s)")
            .description("Ignore first line(s) in the file in the input file")
            .addValidator(StandardValidators.INTEGER_VALIDATOR)
            .required(false)
            .defaultValue("0")
            .build();

    public static final PropertyDescriptor PARSE_NEXT = new PropertyDescriptor.Builder()
            .name("Parse next line(s) as column header")
            .description("Parse next line(s) as column header")
            .addValidator(StandardValidators.INTEGER_VALIDATOR)
            .required(false)
            .defaultValue("1")
            .build();

    public static final PropertyDescriptor DISCARD_INITIAL = new PropertyDescriptor.Builder()
            .name("Discard initial row(s) of data")
            .description("Discard initial row(s) of data")
            .addValidator(StandardValidators.INTEGER_VALIDATOR)
            .required(false)
            .defaultValue("0")
            .build();

    public static final PropertyDescriptor LOAD_AT_MOST = new PropertyDescriptor.Builder()
            .name("Load at most ${number} row(s) of data")
            .description("Load at most ${number} row(s) of data from input file")
            .addValidator(StandardValidators.INTEGER_VALIDATOR)
            .required(false)
            .defaultValue("0")
            .build();

    public static final PropertyDescriptor STORE_BLANK_ROWS = new PropertyDescriptor.Builder()
            .name("Store blank rows")
            .description("Whether to store blank rows or not")
            .addValidator(StandardValidators.BOOLEAN_VALIDATOR)
            .required(false)
            .allowableValues("true", "false")
            .defaultValue("true")
            .build();

    public static final PropertyDescriptor STORE_BLANK_CELLS_AS_NULL = new PropertyDescriptor.Builder()
            .name("Store blank cells as nulls")
            .description("Whether to store blank cells as nulls or not")
            .addValidator(StandardValidators.BOOLEAN_VALIDATOR)
            .required(false)
            .allowableValues("true", "false")
            .defaultValue("true")
            .build();

    public static final PropertyDescriptor STORE_FILE_SOURCE = new PropertyDescriptor.Builder()
            .name("Store file source (file names, URLs) in each row")
            .description("Store file source (file names, URLs) in each row, used when importing multiple sheets.")
            .addValidator(StandardValidators.BOOLEAN_VALIDATOR)
            .required(false)
            .allowableValues("true", "false")
            .defaultValue("false")
            .build();

    public static final PropertyDescriptor SHEETS_TO_IMPORT = new PropertyDescriptor.Builder()
            .name("Worksheets to Import")
            .description("Worksheets to Import, for instance : 0,1,2")
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .required(false)
            .defaultValue("0")
            .build();

    public static final Relationship REL_SUCCESS = new Relationship.Builder().name("success").description("All files are routed to success").build();
    public static final Relationship REL_FAILURE = new Relationship.Builder().name("failure").description("All files are routed to failure").build();

    private List<PropertyDescriptor> properties;
    private Set<Relationship> relationships;


    @Override
    protected void init(final ProcessorInitializationContext context) {
        final List<PropertyDescriptor> properties = new ArrayList<>();
        properties.add(URL);
        properties.add(PORT);
        properties.add(JSON);
        properties.add(PROJECT_NAME);
        properties.add(DELETE_PROJECT);
        properties.add(OUTPUT_FILENAME);
        properties.add(OUTPUT_FILE_EXTENSION);
        properties.add(SHEETS_TO_IMPORT);
        properties.add(IGNORE_FIRST);
        properties.add(PARSE_NEXT);
        properties.add(DISCARD_INITIAL);
        properties.add(LOAD_AT_MOST);
        properties.add(STORE_BLANK_ROWS);
        properties.add(STORE_BLANK_CELLS_AS_NULL);
        properties.add(STORE_FILE_SOURCE);

        this.properties = Collections.unmodifiableList(properties);

        final Set<Relationship> relationships = new HashSet<>();
        relationships.add(REL_SUCCESS);
        relationships.add(REL_FAILURE);
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    protected List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return properties;
    }

    @Override
    public Set<Relationship> getRelationships() {
        return relationships;
    }


    private int applyOperations(String url, Integer port, String projectId, JSONArray json) throws Exception {
        HttpResponse<JsonNode> jsonResponse = Unirest.post(url + ":" + port + "/command/core/apply-operations?project=" + projectId)
                .header("accept", "application/json")
                .field("operations", json)
                .asJson();

        if (jsonResponse.getStatus() != 200) {
            throw new Exception("Openrefine applyOperations responded with status code different than 200 (SUCCESS)");
        }
        return jsonResponse.getStatus();
    }

    private String createProjectFromUpload(InputStream is,
                                           String url,
                                           Integer port,
                                           String projectName,
                                           ArrayList<Integer> sheets,
                                           Integer ignoreLines,
                                           Integer headerLines,
                                           Integer skipDataLines,
                                           Integer loadAtMost,
                                           boolean storeBlankRows,
                                           boolean storeBlankCellsAsNulls,
                                           boolean includeFileSources
    ) throws Exception {

        JSONObject options = new JSONObject();
        options.put("xmlBased", true);
        options.put("sheets", sheets);
        options.put("ignoreLines", ignoreLines);
        options.put("headerLines", headerLines);
        options.put("skipDataLines", skipDataLines);
        options.put("limit", loadAtMost);
        options.put("storeBlankRows", storeBlankRows);
        options.put("storeBlankCellsAsNulls", storeBlankCellsAsNulls);
        options.put("includeFileSources", includeFileSources);

        final ComponentLog logger = getLogger();
        logger.info("JSON Object generated : " + options);
        HttpResponse<JsonNode> jsonResponse = Unirest.post(url + ":" + port + "/command/core/create-project-from-upload")
                .header("accept", "application/json")
                .queryString("options", options)
                .field("project-name", projectName)
                .field("format", "binary/text/xml/xls/xlsx")
                .field("project-file", is, projectName)
                .asJson();

        if (jsonResponse.getStatus() != 302) {
            throw new Exception("Openrefine createProjectFromUpload responded with a code status different than 302 " +
                    "(CREATED) : "
                    + jsonResponse.getStatus());
        }
        String responseHeaderLocation = jsonResponse.getHeaders().get("Location").get(0);
        String projectId;
        if (!responseHeaderLocation.contains("=")) {
            throw new IllegalArgumentException("String " + responseHeaderLocation + " does not contain =");
        }

        String[] parts = responseHeaderLocation.split("=");
        projectId = parts[1];

        return projectId;
    }

    private InputStream exportProjectToCsv(String url, Integer port, String projectId, String outputFileExtension) throws Exception {
        HttpResponse<InputStream> jsonResponse = Unirest.post(url + ":" + port + "/command/core/export-rows")
                .header("accept", "application/json")
                .field("project", projectId)
                .field("format", outputFileExtension)
                .asBinary();

        if (jsonResponse.getStatus() != 200) {
            throw new Exception("Openrefine exportProjectToCsv responded with status code different than 200 (SUCCESS)");
        }
        return jsonResponse.getBody();
    }

    private int deleteProject(String url, Integer port, String projectId) throws Exception {
        HttpResponse<JsonNode> jsonResponse = Unirest.post(url + ":" + port + "/command/core/delete-project")
                .header("accept", "application/json")
                .field("project", projectId)
                .asJson();
        if (jsonResponse.getStatus() != 200) {
            throw new Exception("Openrefine deleteProject responded with status code different than 200 (SUCCESS)");
        }
        return jsonResponse.getStatus();
    }


    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
        final String url = context.getProperty(URL).evaluateAttributeExpressions().getValue();
        final Integer port = context.getProperty(PORT).asInteger();
        final String json = context.getProperty(JSON).getValue();
        final JSONArray jsonParsed = new JSONArray(json);
        final String projectName = context.getProperty(PROJECT_NAME).getValue().toLowerCase();
        final Boolean deleteProject = context.getProperty(DELETE_PROJECT).asBoolean();
        final String outputFileName = context.getProperty(OUTPUT_FILENAME).evaluateAttributeExpressions().getValue();
        final String outputFileExtension = context.getProperty(OUTPUT_FILE_EXTENSION).getValue();
        final Integer ignoreFirstLines = context.getProperty(IGNORE_FIRST).asInteger();
        final Integer parseNextLines = context.getProperty(PARSE_NEXT).asInteger();
        final Integer discardNextLines = context.getProperty(DISCARD_INITIAL).asInteger();
        final Integer loadAtMost = context.getProperty(LOAD_AT_MOST).asInteger();
        final boolean storeBlankRows = context.getProperty(STORE_BLANK_ROWS).asBoolean();
        final boolean storeBlankCellsAsNull = context.getProperty(STORE_BLANK_CELLS_AS_NULL).asBoolean();
        final boolean storeFileSource = context.getProperty(STORE_FILE_SOURCE).asBoolean();
        final String sheetsToImport = context.getProperty(SHEETS_TO_IMPORT).getValue();
        final ArrayList<Integer> sheetToImportInt = new ArrayList<>();

        for (String sheet : sheetsToImport.split(",")) {
            sheetToImportInt.add(parseInt(sheet));
        }

        final ComponentLog logger = getLogger();

        final FlowFile flowFile = session.get();
        FlowFile next = null;
        InputStream is = null;

        if (flowFile == null) {
            return;
        }

        try {

            final long importStart = System.nanoTime();
            String projectId = createProjectFromUpload(
                    session.read(flowFile),
                    url,
                    port,
                    projectName,
                    sheetToImportInt,
                    ignoreFirstLines,
                    parseNextLines,
                    discardNextLines,
                    loadAtMost,
                    storeBlankRows,
                    storeBlankCellsAsNull,
                    storeFileSource
            );
            applyOperations(url, port, projectId, jsonParsed);
            is = exportProjectToCsv(url, port, projectId, outputFileExtension);

            next = session.create();
            next = session.importFrom(is, next);
            final long importNanos = System.nanoTime() - importStart;
            final long importMillis = TimeUnit.MILLISECONDS.convert(importNanos, TimeUnit.NANOSECONDS);
            session.getProvenanceReporter().receive(flowFile, projectId, importMillis);
            next = session.putAllAttributes(next, flowFile.getAttributes());
            next = session.putAttribute(next, "filename", outputFileName + "." + outputFileExtension);
            session.transfer(next, REL_SUCCESS);
            session.remove(flowFile);
            session.commit();

            if(deleteProject){
                deleteProject(url, port, projectId);
            }

        } catch (final Exception e) {
            logger.error("Failed to process file due to {}", new Object[]{e});
            if (next != null) {
                session.remove(next);
            }
            if (flowFile != null) {
                session.transfer(flowFile, REL_FAILURE);
            }
        } finally {
        }
    }
}