#!/bin/sh
BASE=/media/s3ni0r/Storage/Co-workers/Verteego/Code/ml-stack/nifi
NIFI_HOME=$BASE/nifi-assembly/target/nifi-1.1.0-SNAPSHOT-bin/nifi-1.1.0-SNAPSHOT
NAR_HOME=$BASE/verteego-nar-bundles/openrefine
NAR_TARGET=$BASE/verteego-nar-bundles/openrefine/nifi-openrefine-nar/target

$NIFI_HOME/bin/nifi.sh stop
(cd $NAR_HOME && mvn -T 2.0C clean install -DskipTests)
cp $NAR_TARGET/nifi-openrefine-nar-1.0-SNAPSHOT.nar $NIFI_HOME/lib/
$NIFI_HOME/bin/nifi.sh start